package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	rcon "gitlab.com/la-livre/rust-rcon"

	"github.com/gorilla/websocket"
	"github.com/howeyc/gopass"
)

func main() {
	fmt.Print("Address: ")
	addr, err := readline()
	if err != nil {
		panic(err)
	}

	fmt.Print("Port (leave blank for default password 28016): ")
	port, err := readline()
	if err != nil {
		panic(err)
	}

	if port == "" {
		port = "28016"
	}

	fmt.Print("Password: ")
	pass, err := gopass.GetPasswdMasked()
	if err != nil {
		panic(err)
	}

	// Block to test if credntials are valid by trying to issue a command
	c, _, err := websocket.DefaultDialer.Dial("ws://"+addr+":"+port+"/"+string(pass), nil)
	if err != nil {
		panic(err)
	}
	c.WriteJSON(rcon.MSG{Message: "server.seed"})
	msgChannel := make(chan rcon.MSG, 1)
	errChannel := make(chan error, 1)

	go func() {
		var m rcon.MSG
		for {
			if err := c.ReadJSON(&m); err != nil {
				errChannel <- err
			}
			msgChannel <- m
		}
	}()
	select {
	case <-msgChannel:
		fmt.Println("Connection established!")
		break
	case err := <-errChannel:
		panic(err)
	case <-time.After(5 * time.Second):
		panic("Couldn't receive a response to the test command. As the server didn't respond to the first request, this program will shutdown")
	}
	// End of the block

	r, err := rcon.NewRCON(addr, port, string(pass), 5*time.Second)
	if err != nil {
		panic(err)
	}

	for {
		fmt.Print("RCON@" + addr + ">")
		s, err := readline()
		if err != nil {
			panic(err)
		}
		message, err := r.SendCommand(s)
		if err != nil {
			if err == rcon.ErrTimeout {
				fmt.Println("Command timeout. Maybe the command is invalid?")
			} else {
				panic(err)
			}
		}
		fmt.Println(message.Message)
	}
}

func readline() (string, error) {
	reader := bufio.NewReader(os.Stdin)
	s, err := reader.ReadString(byte('\n'))
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(s), nil
}
