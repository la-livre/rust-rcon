package rcon

import (
	"errors"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

// MSG holds the response
type MSG struct {
	Message    string
	Identifier int
	Type       string
}

var (
	// ErrTimeout is returned when the server didn't respond in time
	ErrTimeout = errors.New("Didn't receive a response in time")
)

// RCON hold all usefull infos regarding the server's connection
type RCON struct {
	C           *websocket.Conn
	Addr        string
	Port        string
	Pass        string
	LastMessage int
	sync.Mutex
	Timeout time.Duration
}

// NewRCON returns a new connection to the RCON server. You should only need one per server
func NewRCON(address, port, password string, timeout time.Duration) (*RCON, error) {
	var r RCON

	r.Addr = address
	r.Port = port
	r.Pass = password
	r.Timeout = timeout
	r.LastMessage = 0

	return &r, nil
}

// SendCommand sends the provided command and returns the reponse. If no response is given after the rcon's timeout if finished, then it returns an error
func (r *RCON) SendCommand(cmd string) (MSG, error) {
	r.Lock()
	var m MSG

	c, _, err := websocket.DefaultDialer.Dial("ws://"+r.Addr+":"+r.Port+"/"+r.Pass, nil)
	if err != nil {
		return m, err
	}
	r.C = c

	defer r.Unlock()
	defer r.C.Close()

	/*
		In the case we didn't get the returning value of the previous command in time, we could get its value later while thinking it's the current command's
		To avoid that, we implement a dynamic Identifier value used to check that the return value is indeed the command's we just sat
	*/
	var current int
	switch r.LastMessage {
	case 127: // Reset to zero after some time to avoid an integer overflow error (It would take a lot of time to get there, but we never now)
		current = 0
	default:
		current = r.LastMessage + 1
	}

	if err := r.C.WriteJSON(MSG{Message: cmd, Identifier: current}); err != nil {
		return m, err
	}

	msgChannel := make(chan MSG, 1)
	errChannel := make(chan error, 1)

	go func() {
		for {
			if err := r.C.ReadJSON(&m); err != nil {
				errChannel <- err
			}
			if m.Identifier == current { // Break out of the loop if the identifier is correct, else, keep looking
				msgChannel <- m
				break
			}
		}
	}()
	select {
	case res := <-msgChannel:
		return res, nil
	case err := <-errChannel:
		return m, err
	case <-time.After(r.Timeout):
		return m, ErrTimeout
	}
}
